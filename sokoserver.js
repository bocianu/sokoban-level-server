const db = require('better-sqlite3')('sokoban.db');

const getWhere = q => ' WHERE 1=1 ' + (q.x?` AND x <= ${q.x}`:'') + (q.y?` AND y <= ${q.y}`:'') + (q.set?` AND levels_set_id = ${q.set}`:'');

const getLevel = q => {
	const sql = `SELECT l.x, l.y, l.content, l.levels_set_id as set_id, l.ordinal_number as ord, s.name as setname, a.name as author FROM levels l INNER JOIN levels_set s ON l.levels_set_id = s.id INNER JOIN author a ON s.author_id = a.id`;
	const order = ` ORDER BY l.id ASC`;
	const levels = db.prepare(sql + getWhere(q) + order).all();
	const num = q.num ? q.num : Math.floor(Math.random()*levels.length);
	return levels[num];
}

const getCount = q => {
	const sql = `SELECT count(id) FROM levels`;
	return db.prepare(sql + getWhere(q)).pluck().get();
}

module.exports = {
	getLevel,
	getCount
}
