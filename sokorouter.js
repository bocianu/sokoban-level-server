const Router = require('koa-router');
const _ = require('lodash');
const soko = require('./sokoserver.js');
const router = new Router({prefix: '/soko'});

router.get('/count', async (ctx, next) => {
	ctx.type = 'application/octet-stream';
	ctx.body = soko.getCount(ctx.query); 
	await next();
});

router.get('/level', async (ctx, next) => {
	ctx.type = 'application/octet-stream';
	const level = soko.getLevel(ctx.query);
	if (level) { ctx.body = level } 
	else { ctx.status = 404 };
	await next();
});

module.exports = router;
