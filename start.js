const Koa = require('koa');
const srouter = require('./sokorouter.js');

const app = new Koa();
app.use(srouter.routes());
console.log('server started');
app.listen(2136);